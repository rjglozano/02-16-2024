import java.time.*;

public class Flight {
    static LocalDateTime arrival(ZonedDateTime departure, Duration flightTime) {
        ZoneId zId = ZoneId.systemDefault();
        return departure.plus(flightTime).withZoneSameInstant(zId).toLocalDateTime();
    }

    public static void main(String[] args) {

        ZonedDateTime harareDeparture = ZonedDateTime.of(LocalDateTime.of(2015, Month.DECEMBER, 1, 9, 15),
                ZoneId.of("Africa/Harare"));
        Duration harareFlightTime = Duration.ofHours(20);
        LocalDateTime arrival = arrival(harareDeparture, harareFlightTime);
        System.out.println(arrival);
    }

}
